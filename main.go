package main

import (
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/maspratama/auth/infra"
)

func main() {

	// load timexone
	localtime, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}
	log.Println(localtime)

	// estabileshed db
	infra.ConnectToDB()

	app := fiber.New()

	app.Listen(":8080")
}
