package infra

import (
	"errors"
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

// get db params from .env
func getPostgresConfig() string {
	dbType := GetEnvVariable("DB_TYPE")
	if dbType == "" {
		dbType = "postgres"
	}
	dbHost := GetEnvVariable("DB_HOST")
	if dbHost == "" {
		dbHost = "localhost"
	}

	dbPort := GetEnvVariable("DB_PORT")
	if dbPort == "" {
		dbPort = "5432"
	}

	dbUser := GetEnvVariable("DB_USER")
	if dbUser == "" {
		dbUser = "-"
	}

	dbName := GetEnvVariable("DB_NAME")
	if dbName == "" {
		dbName = "postgres"
	}

	dbPass := GetEnvVariable("DB_PASS")

	dataBase := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable TimeZone=Asia/Jakarta",
		dbHost,
		dbPort,
		dbUser,
		dbName,
		dbPass,
	)
	return dataBase
}

// connection to DB
var DB *gorm.DB

func ConnectToDB() (*gorm.DB, error) {
	connection := getPostgresConfig()
	DB, err := gorm.Open(postgres.Open(connection), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})

	if err != nil {
		return DB, errors.New("Database connection failed: " + err.Error())
	} else {
		log.Printf("Connected to %s database on port: %s ", GetEnvVariable("DB_NAME"), GetEnvVariable("DB_PORT"))
	}
	return DB, err
}
